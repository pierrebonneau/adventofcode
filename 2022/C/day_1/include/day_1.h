#if !defined(DAY_1_H)
#define DAY_1_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int day_1_1(FILE *input_file)
{
	int LINE_SIZE = 15;
	char line[LINE_SIZE];

	int max_calories = 0;
	int sum_calories = 0;
	int calories = 0;
	int elf = 0;

	while (fgets(line, LINE_SIZE, input_file) != NULL)
	{
		if (sscanf(line, "%d", &calories) > 0)
		{
			printf("Add to sum\n");
			sum_calories += calories;
			printf("%d\n", sum_calories);
		}
		else
		{
			elf++;
			if (sum_calories > max_calories)
			{
				printf("New max\n");
				max_calories = sum_calories;
			}
			sum_calories = 0;
			printf("Next elf\n");
		}
	}
	printf("%d\n", max_calories);

	return (0);
}

int day_1_2(FILE *input_file)
{
	int LINE_SIZE = 15;
#define TOP_CAL 3

	char line[LINE_SIZE];
	int top_calories[TOP_CAL] = {0};
	int idx_min = 0;
	int sum_calories = 0;
	int calories = 0;
	int elf = 0;

	while (fgets(line, LINE_SIZE, input_file) != NULL)
	{
		if (sscanf(line, "%d", &calories) > 0)
		{
			printf("Add to sum\n");
			sum_calories += calories;
			printf("%d\n", sum_calories);
		}
		else
		{
			elf++;
			if (sum_calories > top_calories[idx_min])
			{
				printf("New top : %d\n", sum_calories);
				top_calories[idx_min] = sum_calories;
				idx_min = 0;
				for (int i = 1; i < TOP_CAL; i++)
				{
					if (top_calories[i] < top_calories[idx_min])
						idx_min = i;
				}
			}
			sum_calories = 0;
			printf("Next elf\n");
		}
	}
	elf++;
	if (sum_calories > top_calories[idx_min])
	{
		printf("New top : %d\n", sum_calories);
		top_calories[idx_min] = sum_calories;
		idx_min = 0;
		for (int i = 1; i < TOP_CAL; i++)
		{
			if (top_calories[i] < top_calories[idx_min])
				idx_min = i;
		}
	}

	int sum_top3 = 0;
	for (int i = 0; i < TOP_CAL; i++)
		sum_top3 += top_calories[i];
	printf("%d\n", sum_top3);

	return (0);
}

#endif // DAY_1_H
