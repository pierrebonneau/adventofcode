#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "day_2.h"

int main(int argc, char **argv)
{
	FILE *input_file;

	if (argc < 3)
	{
		fprintf(stderr, "Input parameters are missing.\n");
		exit(EXIT_FAILURE);
	}
	if ((strcmp("1", argv[1]) != 0) && (strcmp("2", argv[1]) != 0))
	{
		fprintf(stderr, "Select part 1 or 2 of the problem.\n");
		exit(EXIT_FAILURE);
	}
	input_file = fopen(argv[2], "r");
	if (input_file == 0)
	{
		fprintf(stderr, "Cannot open input file %s.\n", argv[1]);
		exit(EXIT_FAILURE);
	}

	if (strcmp("1", argv[1]) == 0)
	{
		day_2_1(input_file);
	}
	else
	{
		day_2_2(input_file);
	}

	return (0);
}