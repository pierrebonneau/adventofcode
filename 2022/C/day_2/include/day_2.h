#if !defined(DAY_2_H_INCLUDED)
#define DAY_2_H_INCLUDED

char standardize_choice(char c)
{
    if (c == 'A' || c == 'X')
        c = 'R';
    else if (c == 'B' || c == 'Y')
        c = 'P';
    else if (c == 'C' || c == 'Z')
        c = 'S';

    return c;
}

int choice_value(char c)
{
    int value = 0;
    switch (c)
    {
    case 'R':
        value = 1;
        break;
    case 'P':
        value = 2;
        break;
    case 'S':
        value = 3;
        break;

    default:
        break;
    }
    return value;
}

char tactical_choice(char opponent_choice, char tactic)
{
    char choice = opponent_choice;
    switch (tactic)
    {
    case 'X': // LOSS
        switch (opponent_choice)
        {
        case 'R':
            choice = 'S';
            break;
        case 'P':
            choice = 'R';
            break;
        case 'S':
            choice = 'P';
            break;
        default:
            break;
        }
        break;
    case 'Z': // WIN
        switch (opponent_choice)
        {
        case 'R':
            choice = 'P';
            break;
        case 'P':
            choice = 'S';
            break;
        case 'S':
            choice = 'R';
            break;
        default:
            break;
        }
        break;
    case 'Y': // DRAW
        choice = opponent_choice;

    default:
        break;
    }

    return choice;
}

int day_2_1(FILE *input_file)
{
    int LEN_LINE = 6;
    char line[LEN_LINE];

    char player_choice;
    char opponent_choice;
    int total_score = 0;
    int round_score = 0;

    while (fgets(line, LEN_LINE, input_file) != NULL)
    {
        opponent_choice = standardize_choice(line[0]);
        player_choice = standardize_choice(line[2]);
        printf("%c vs. %c\n", player_choice, opponent_choice);
        if (player_choice == opponent_choice)
            round_score = 3; // DRAW
        else if ((player_choice == 'R' && opponent_choice == 'S') || (player_choice == 'P' && opponent_choice == 'R') || (player_choice == 'S' && opponent_choice == 'P'))
            round_score = 6; // WIN
        else
            round_score = 0; // LOSS
        round_score += +choice_value(player_choice);
        printf("Round score : %d\n", round_score);
        total_score += round_score;
    }

    printf("Total score : %d", total_score);

    return 0;
}

int day_2_2(FILE *input_file)
{
    int LEN_LINE = 6;
    char line[LEN_LINE];

    char player_choice;
    char opponent_choice;
    int total_score = 0;
    int round_score = 0;

    while (fgets(line, LEN_LINE, input_file) != NULL)
    {
        opponent_choice = standardize_choice(line[0]);
        player_choice = tactical_choice(opponent_choice, line[2]);
        printf("%c vs. %c\n", player_choice, opponent_choice);
        if (player_choice == opponent_choice)
            round_score = 3; // DRAW
        else if ((player_choice == 'R' && opponent_choice == 'S') || (player_choice == 'P' && opponent_choice == 'R') || (player_choice == 'S' && opponent_choice == 'P'))
            round_score = 6; // WIN
        else
            round_score = 0; // LOSS
        round_score += +choice_value(player_choice);
        printf("Round score : %d\n", round_score);
        total_score += round_score;
    }

    printf("Total score : %d", total_score);

    return 0;
}

#endif // DAY_2_H_INCLUDED
