import sys
import numpy as np

def main():
	with open(sys.argv[-1], "r") as infile:
		lines = infile.read().strip().split()

	report = np.array([list(l) for l in lines]).astype(np.int64)

	o2_bit_criterion = lambda ones, zeros: 1 if ones >= zeros else 0
	co2_bit_criterion = lambda ones, zeros: 1 if ones < zeros else 0

	o2_rating = find_gas_rating(report, o2_bit_criterion)
	co2_rating = find_gas_rating(report, co2_bit_criterion)

	life_support_rating = o2_rating * co2_rating
	print(life_support_rating)

def find_gas_rating(report, bit_criterion):
	candidates = report.copy()
	for i in range(report.shape[1]):
		ones = np.sum(candidates[:, i])
		zeros = candidates.shape[0] - ones

		criterion = bit_criterion(ones, zeros)
		candidates = candidates[candidates[:, i] == criterion]

		if candidates.shape[0] == 1:
			rating = "".join(candidates.astype(str).flatten().tolist())
			return int(rating, 2)

if __name__ == '__main__':
	main()

# gamma_rate = int("".join(gamma_rate), 2)
# epsilon_rate = int("".join(epsilon_rate), 2)

# power_consumption = gamma_rate * epsilon_rate

# print(gamma_rate, epsilon_rate)
# print(power_consumption)