import sys
import numpy as np


with open(sys.argv[-1], "r") as infile:
	lines = infile.read().strip().split()

report = np.array([list(l) for l in lines]).astype(np.int64)

gamma_rate = []
epsilon_rate = []

for i in range(report.shape[1]):
	ones = np.sum(report[:, i])
	zeros = report.shape[0] - ones
	g = 1 if ones > zeros else 0
	e = 1 if ones < zeros else 0
	gamma_rate.append(str(g))
	epsilon_rate.append(str(e))

gamma_rate = int("".join(gamma_rate), 2)
epsilon_rate = int("".join(epsilon_rate), 2)

power_consumption = gamma_rate * epsilon_rate

print(gamma_rate, epsilon_rate)
print(power_consumption)