import sys
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

from sklearn.linear_model import LinearRegression
def main():
	with open(sys.argv[-1], "r") as infile:
		lines = infile.read()
	positions = np.array([int(x) for x in lines.split(",")])
	min_pos = np.min(positions)
	max_pos = np.max(positions)

	print("Mean:",  np.mean(positions))
	print("Std.:",  np.std(positions))

	costs = []
	for i in range(1 + max_pos - min_pos):
		costs.append(sum([j for j in range(i+1)]))
	
	consumptions = []
	for p in range(min_pos, max_pos + 1):
		consumptions.append(sum([costs[abs(p - x)] for x in positions]))
	
	print("Min. consumption:", min(consumptions))
	print("Pos. for min. consumption:", min_pos + consumptions.index(min(consumptions)))

	# print(costs)
	# print(consumptions)

if __name__ == '__main__':
	main()