import sys
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

from sklearn.linear_model import LinearRegression
def main():
	with open(sys.argv[-1], "r") as infile:
		lines = infile.read()
	positions = np.array([int(x) for x in lines.split(",")])
	min_pos = np.min(positions)
	max_pos = np.max(positions)
	costs = []
	for i in range(min_pos, max_pos):
		costs.append(np.sum(np.absolute(i - positions)))
	print("Median:",  np.median(positions))
	print("Mean:",  np.mean(positions))
	print("Min. cost at:", costs.index(min(costs)))
	print("Min. cost:", min(costs))

if __name__ == '__main__':
	main()