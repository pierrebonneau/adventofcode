import sys
from collections import Counter


def main():
	with open(sys.argv[-1], "r") as infile:
		lines = infile.read().split("\n\n")
		polymer = lines.pop(0)
		rules = {rule.split(" -> ")[0]: rule.split(" -> ")[1] for rule in "".join(lines).split("\n")}

	STEPS = 10
	for i in range(STEPS):
		new_polymer = ""
		for j in range(len(polymer) - 1):
			new_polymer = new_polymer + polymer[j] + rules[polymer[j:j+2]]
		polymer = new_polymer + polymer[-1]

	counter = Counter(polymer)

	most_freq = counter.most_common(1)[0][1]
	least_freq = counter.most_common()[-1][1]

	print(most_freq - least_freq)


if __name__ == '__main__':
	main()