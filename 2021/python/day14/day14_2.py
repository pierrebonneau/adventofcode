import functools
import sys
from collections import Counter, defaultdict



def main():
	with open(sys.argv[-1], "r") as infile:
		lines = infile.read().split("\n\n")
	
	template = lines.pop(0)
	global rules
	rules = {rule.split(" -> ")[0]: rule.split(" -> ")[1] for rule in "".join(lines).split("\n")}

	STEPS = 40
	
	count_template = defaultdict(lambda: 0)
	count_single_char = defaultdict(lambda: 0)
	count_pairs = Counter({})
	
	for k, v in rules.items():
		rules[k] = []
		rules[k].append(k[0] + v)
		rules[k].append(v + k[1])

	for c in template:
		count_template[c] += 1

	for i in range(len(template) - 1):
		current_pair = template[i:i+2]
		count_pairs += insert(current_pair, STEPS)
	
	count_single_char[template[0]] += 1
	count_single_char[template[-1]] += 1
	for pair, count in count_pairs.items():
		for p in pair:
			count_single_char[p] += count
	for char, count in count_single_char.items():
		count_single_char[char] //= 2

	count_single_char = Counter(count_single_char)
	most_freq = count_single_char.most_common(1)[0][1]
	least_freq = count_single_char.most_common()[-1][1]

	print(most_freq - least_freq)

@functools.lru_cache(maxsize=None)
def insert(pair, repetitions):
	if repetitions > 0:
		repetitions -= 1
		left = insert(rules[pair][0], repetitions)
		right = insert(rules[pair][1], repetitions)
		return  left + right
	else:
		return Counter({pair: 1})

if __name__ == '__main__':
	main()
