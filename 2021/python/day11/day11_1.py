import sys
import numpy as np
from scipy.ndimage import generic_filter


def main():
	with open(sys.argv[-1], "r") as infile:
		octopuses = list(infile.read().replace("\n", ""))
		octopuses = np.array(octopuses, dtype=np.int64).reshape((10, 10))

	STEPS = 100

	flashed = np.zeros(octopuses.shape, dtype=bool)
	new_flash = np.zeros(octopuses.shape, dtype=bool)
	total_flashes = 0

	for i in range(STEPS):
		flashed[:] = False
		octopuses += 1

		chain_reaction = True
		while chain_reaction:
			new_flash[np.logical_and(octopuses > 9, flashed == False)] = True
			flashed = np.logical_or(flashed, new_flash)
			energy_gain = generic_filter(new_flash,
										footprint=np.ones((3, 3)),
										function= lambda x: np.sum(x),
										mode="constant",
										cval=False,
										output=np.int64)
			# print(energy_gain)
			octopuses = octopuses + energy_gain
			chain_reaction = np.any(new_flash)
			new_flash[:] = False
		total_flashes += np.sum(flashed)
		octopuses[octopuses > 9] = 0

	print(total_flashes)
"""
	increase energy_lvl by 1
	all octo > 9 -> flashes
	create mask bool: Flashes True 
	use generic filter to increment by one energy level of neighbours flashes=true

"""

if __name__ == '__main__':
	main()