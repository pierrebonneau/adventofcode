import sys
import numpy as np


def main():

	with open(sys.argv[-1]) as infile:
		lines = infile.read().strip().split("\n\n")
	
	draw = [int(i) for i in lines[0].split(",")]
	grids = [[g.split() for g in grid.split("\n")] for grid in lines[1:]]
	grids = [np.array(grid).astype(np.int64) for grid in grids]
	
	grids_drawn = [None for g in range(len(grids))]
	
	winner = 0
	drawn = []
	while winner != 1 and len(draw) > 0:
		drawn.append(draw.pop(0))
		for i in range(len(grids)):
			grids_drawn[i] = np.isin(grids[i], drawn)

			complete_row = np.all(grids_drawn[i], axis= 1)
			complete_col = np.all(grids_drawn[i], axis= 0)
			if np.any(complete_row) or np.any(complete_col):
				winner = 1
				score = sum(grids[i][~grids_drawn[i]]) * drawn[-1]
	print(score)


if __name__ == '__main__':
	main()