import sys
from collections import defaultdict

def main():
	with open(sys.argv[-1], "r") as infile:
		connections = infile.read().split("\n")

	graph = defaultdict(lambda: set())
	for c in connections:
		node_a, node_b = c.split("-")
		graph[node_a].add(node_b)
		graph[node_b].add(node_a)

	paths = find_paths(graph, "start", "end", set())
	print(len(paths))

def find_paths(graph, start, end, visited):
	paths = []
	if graph[start]:
		for node in graph[start]:
			if node == end:
				paths.append([start, end])
			elif node not in visited or node.isupper():
				visited_copy = visited.copy()
				visited_copy.add(start)
				subpaths = find_paths(graph, node, end, visited_copy)
				for sp in subpaths:
					sp.insert(0, start)
					paths.append(sp)
	return paths



if __name__ == '__main__':
	main()