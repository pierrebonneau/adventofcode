import sys
from collections import defaultdict

def main():
	with open(sys.argv[-1], "r") as infile:
		connections = infile.read().split("\n")

	graph = defaultdict(lambda: set())
	for c in connections:
		node_a, node_b = c.split("-")
		if node_b == "start": node_a, node_b = node_b, node_a
		if node_a != "end":
			graph[node_a].add(node_b)
		if node_b != "end" and node_a != "start":
			graph[node_b].add(node_a)
	
	paths = find_paths(graph, "start", "end", defaultdict(lambda: 0), can_twice = True)
	
	for p in paths:
		print(",".join(p))
	print(len(paths))

def find_paths(graph, start, end, visited, can_twice):
	paths = []
	if graph[start]:
		for node in graph[start]:
			if node == end:
				paths.append([start, end])
			elif node.isupper() or (visited[node] < 2 and can_twice) or (visited[node] < 1):
				can_twice_copy = can_twice
				if visited[node] == 1 and node.islower() and can_twice:
					can_twice_copy = False
				visited_copy = visited.copy()
				visited_copy[node] += 1
				subpaths = find_paths(graph, node, end, visited_copy, can_twice_copy)
				for sp in subpaths:
					sp.insert(0, start)
					paths.append(sp)

	return paths



if __name__ == '__main__':
	main()