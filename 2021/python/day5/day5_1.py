import sys
import numpy as np


def main():
	
	with open(sys.argv[-1]) as infile:
		coords = infile.read().replace("\n", ",").replace(" -> ", ",").split(",")
	
	coords = [int(c) for c in coords]
	max_ = max(coords) + 1
	diagram = np.zeros((max_, max_))

	for i in range(0, len(coords) - 4, 4):
		x1 = coords[i]
		y1 = coords[i + 1]
		x2 = coords[i + 2]
		y2 = coords[i + 3]
	
		if x1 == x2 or y1 == y2:
			if x2 < x1: x1, x2 = x2, x1
			if y2 < y1: y1, y2 = y2, y1
			for j in range(x1, x2 + 1):
				for k in range(y1, y2 + 1):
					diagram[j, k] = diagram[j, k] + 1
	print(np.sum(diagram > 1))
	# diagram = np.char.strip(diagram.T.astype(str), ".0")
	# diagram[diagram == ""] = "."
	# print(diagram)

if __name__ == "__main__":
	main()