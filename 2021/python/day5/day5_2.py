import sys
import numpy as np
from skimage.draw import line


def main():
	
	with open(sys.argv[-1]) as infile:
		coords = infile.read().replace("\n", ",").replace(" -> ", ",").split(",")
	
	coords = [int(c) for c in coords]
	max_ = max(coords) + 1
	diagram = np.zeros((max_, max_))

	for i in range(0, len(coords), 4):
		x1 = coords[i]
		y1 = coords[i + 1]
		x2 = coords[i + 2]
		y2 = coords[i + 3]
		# print(x1,y1,x2,y2)
		rows, cols = line(x1, y1, x2, y2)
		diagram[rows, cols] = diagram[rows, cols] + 1
	print(np.sum(diagram > 1))
	# diagram = np.char.strip(diagram.T.astype(str), ".0")
	# diagram[diagram == ""] = "."
	# print(diagram)

if __name__ == "__main__":
	main()