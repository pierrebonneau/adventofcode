import sys
import numpy as np


def main():
	with open(sys.argv[-1], "r") as infile:
		coords, folds = infile.read().split("\n\n")
	coords = coords.replace("\n", ",").split(",")
	cc = [int(i) for i in coords[0::2]]
	rr = [int(j) for j in coords[1::2]]

	x_max = max(cc) + 1
	y_max = max(rr) + 1

	transparent = np.zeros((y_max, x_max))
	# transparent = np.repeat(".", y_max * x_max).reshape((y_max, x_max))
	for i in range(len(rr)):
		transparent[rr[i], cc[i]] = 1

	folds = [tuple(f.replace("fold along ", "").split("=")) for f in folds.split("\n")]

	transparent = fold(transparent, folds[0][0], int(folds[0][1]))
	print(transparent)
	print(np.sum(transparent > 0))	

def fold(array, axis, idx):
	idx = int(idx)
	if axis == "x":
		arr_left = array[:, :idx]
		arr_right = np.fliplr(array[:, idx+1:])
		if arr_right.shape[1] > arr_left.shape[1]:
			arr_left, arr_right = arr_right, arr_left
		arr_left[:, -len(arr_right):] = arr_left[:, -len(arr_right):] + arr_right
		folded = arr_left
	elif axis == "y":
		arr_top = array[:idx, :]
		arr_bot = np.flipud(array[idx+1:, :])
		if arr_bot.shape[0] > arr_top.shape[0]:
			arr_top, arr_bot = arr_bot, arr_top
		arr_top[-len(arr_bot):, :] = arr_top[-len(arr_bot):, :] + arr_bot
		folded = arr_top
	return folded

if __name__ == '__main__':
	main()