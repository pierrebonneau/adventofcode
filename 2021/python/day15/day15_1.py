import sys
import numpy as np


def main():
	sys.setrecursionlimit(10**6)
	
	with open(sys.argv[-1], "r") as infile:
		risk_map = np.array([list(line) for line in infile.read().split("\n")], dtype = np.int64)
	
	start = (0, 0)
	end = (risk_map.shape[0] - 1, risk_map.shape[1] - 1)

	explored = np.zeros(risk_map.shape).astype(bool)
	lowest_risk_lvl = np.zeros(risk_map.shape)
	lowest_risk_lvl = lowest_risk_lvl + np.inf
	lowest_risk_lvl[start] = 0
	predecessor = np.empty(risk_map.shape, dtype=tuple)

	while not np.all(explored):
		x, y = get_min_coords(lowest_risk_lvl, explored)
		explored[x, y] = True
		for x1, y1 in [(x-1, y), (x+1, y), (x, y-1), (x, y+1)]:
			if (0 <= x1 < risk_map.shape[0]) and (0 <= y1 < risk_map.shape[1]) and not explored[x1, y1]:
				if lowest_risk_lvl[x1, y1] > lowest_risk_lvl[x, y] + risk_map[x1, y1]:
					lowest_risk_lvl[x1, y1] = lowest_risk_lvl[x, y] + risk_map[x1, y1]
					predecessor[x1, y1] = (x, y)

	print(lowest_risk_lvl)

def get_min_coords(array, mask):
	results = np.where((array == np.amin(array[mask == False])) & (mask == False))
	return list(zip(results[0], results[1]))[0]


if __name__ == "__main__":
	main()