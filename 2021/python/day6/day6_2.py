import numpy as np
import sys


def main():
	DAYS = 256
	with open(sys.argv[-1], "r") as infile:
		lines = infile.read()
	
	fish_pop= [0 for i in range(9)]
	for x in lines.split(","):
		fish_pop[int(x)] += 1
	print(fish_pop)
	for i in range(DAYS):
		newborns = fish_pop[0]
		
		fish_pop[0:8] = fish_pop[1:9]
		fish_pop[6] += newborns # In reality the ones that gave birth
		fish_pop[8] = newborns
		print(fish_pop)

	print(sum(fish_pop))
	
if __name__ == '__main__':
	main()