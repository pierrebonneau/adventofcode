import numpy as np
import sys


def main():
	
	with open(sys.argv[-1], "r") as infile:
		lines = infile.read()
	
	fish_pop = np.array(lines.split(","), dtype = np.int64)
	print(fish_pop)
	for i in range(80):

		newborns = sum(fish_pop == 0)
		fish_pop -= 1
		fish_pop[fish_pop < 0] = 6

		fish_pop = np.append(fish_pop, [8 for i in range(newborns)], 0)

	print(fish_pop.shape)
	
if __name__ == '__main__':
	main()