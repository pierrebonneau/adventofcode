import sys

window = 3

with open(sys.argv[-1], "r") as infile:
	lines = [int(i) for i in infile.read().split()]
	sums = []
	for i in range(0, len(lines) + 1 - window):
		sums.append(sum(lines[i:i+window]))
	print(sums)
	variations = [(sums[j] - sums[j-1]) > 0 for j in range(1, len(sums))]
	
	print(sum(variations))