import sys

with open(sys.argv[-1], "r") as infile:
	lines = infile.read().split()
	variations = []
	for i in range(1, len(lines)):
		var = int(lines[i]) - int(lines[i - 1])
		variations.append(var > 0)
	print(sum(variations))
		
