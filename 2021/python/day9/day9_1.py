import sys
import numpy as np
from scipy import ndimage


def main():
	with open(sys.argv[-1], "r") as infile:
		heightmap = np.array([list(l) for l in infile.read().split("\n")], dtype=np.int64)

	cval = np.max(heightmap) + 1
	footprint = np.array([[0, 1, 0],
						  [1, 1, 1],
						  [0, 1, 0]])
	result = ndimage.generic_filter(heightmap,
									function=ismin,
									footprint=footprint,
									mode="constant", 
									cval=cval,
									output=bool,
									extra_keywords={"max_height": cval}
									)
	print(np.sum(heightmap[result] + 1))

def ismin(values, max_height):
	x = values[2]
	values[2] = max_height
	return all(x < values)

if __name__ == '__main__':
	main()