import sys
import numpy as np

from scipy import ndimage


def main():
	with open(sys.argv[-1], "r") as infile:
		heightmap = np.array([list(l) for l in infile.read().split("\n")], dtype=np.int64)

	basinmap = BasinMap(heightmap)
	basinmap.define_basins()
	basin = 0

	basins, sizes = np.unique(basinmap.basinmap, return_counts=True)
	sizes = sizes.tolist()
	sizes.pop(0) #remove edges
	results = 1
	for i in range(3):
		results *= sizes.pop(sizes.index(max(sizes)))

	print(results)


class BasinMap():
	def __init__(self, heightmap):
		self.heightmap = heightmap
		self.mask = heightmap == 9
		self.basinmap = np.zeros(heightmap.shape)
		self.basins = 0

	def define_basins(self):
		while not np.all(self.mask):
			for rr, cc in np.transpose(np.where(~self.mask)):
				self.basins += 1
				self.fill_basin(rr, cc)
				break

	def fill_basin(self, x, y):
		if x >= 0 and x < self.heightmap.shape[0] and y >= 0 and y < self.heightmap.shape[1]:
			if not self.mask[x, y]:
				self.basinmap[x, y] = self.basins
				self.mask[x, y] = True
				self.fill_basin(x-1, y)
				self.fill_basin(x+1, y)
				self.fill_basin(x, y-1)
				self.fill_basin(x, y+1)



if __name__ == '__main__':
	main()