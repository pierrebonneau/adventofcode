import sys
from collections import defaultdict

with open(sys.argv[-1], "r") as infile:
	instructions = defaultdict(lambda: 0)
	lines = infile.read().split("\n")
	for i in lines:
		k, v = i.split()
		instructions[k] += int(v)
print(lines)
print(instructions)
print(instructions["forward"] * (instructions["down"] - instructions["up"]))