import sys
from collections import defaultdict

with open(sys.argv[-1], "r") as infile:
	instructions = defaultdict(lambda: 0)
	lines = infile.read().split("\n")

instructions = [tuple(i.split()) for i in lines]	
aim = 0
depth = 0
hpos = 0

for k, v in instructions:
	v = int(v)
	if k == "forward":
		hpos += v
		depth += aim * v
	elif k == "up":
		aim -= v
	else: # k == "down"
		aim += v
print(depth * hpos)
