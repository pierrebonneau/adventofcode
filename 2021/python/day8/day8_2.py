import sys
from itertools import chain

def main():
	with open(sys.argv[-1], "r") as infile:
		lines = infile.read().split("\n")

	total = 0

	for l in lines:
		patterns, outputs = l.split(" | ")
		patterns = patterns.split(" ")
		outputs = outputs.split(" ")

		digit2seg = {}
		six_seg_digits = []
		five_seg_digits = []
		for p in patterns:
			if len(p) == 2:
				digit2seg[1] = set(p)
			elif len(p) == 4:
				digit2seg[4] = set(p)
			elif len(p) == 3:
				digit2seg[7] = set(p)
			elif len(p) == 7:
				digit2seg[8] = set(p)
			elif len(p) == 5:
				five_seg_digits.append(set(p))
			else:
				six_seg_digits.append(set(p))
		
		for pattern in six_seg_digits:
			if digit2seg[4].issubset(pattern):
				digit2seg[9] = pattern
				six_seg_digits.remove(pattern)
		
		for pattern in five_seg_digits:
			if not pattern.issubset(digit2seg[9]):
				digit2seg[2] = pattern
				five_seg_digits.remove(pattern)
		for pattern in five_seg_digits:
			for ssd in six_seg_digits:
				if pattern.issubset(ssd):
					digit2seg[5] = pattern
					digit2seg[6] = ssd
					five_seg_digits.remove(pattern)
					six_seg_digits.remove(ssd)
		digit2seg[3] = five_seg_digits.pop()
		digit2seg[0] = six_seg_digits.pop()

		seg2digit = {}
		for k, v in digit2seg.items():
			seg2digit["".join(sorted(list(v)))] = k
		
		total += int("".join([str(seg2digit["".join(sorted(op))]) for op in outputs]))

	print(total)


if __name__ == '__main__':
	main()