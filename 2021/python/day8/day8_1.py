import sys
from itertools import chain

def main():
	with open(sys.argv[-1], "r") as infile:
		lines = infile.read().split("\n")
	outputs = list(chain(*[l.split(" | ")[1].split(" ") for l in lines]))
	counter = 0
	for o in outputs:
		if len(o) in [2, 3, 4, 7]:
			counter += 1

	print(counter)


if __name__ == '__main__':
	main()