import sys
from collections import defaultdict

def main():
	with open(sys.argv[-1], "r") as infile:
		lines = infile.read().split()

	opening_chars = ["(", "[", "{", "<"]
	closing_chars = {"(": ")", "[": "]", "{": "}", "<": ">"}
	points = {")": 3, "]": 57, "}": 1197, ">": 25137}
	line_status = []
	illegal_chars = defaultdict(lambda: 0)

	for line in lines:
		expected_closing = []
		status = "correct"
		if line[0] in opening_chars:
			expected_closing.append(closing_chars[line[0]])
			for i in range(1, len(line)):
				if line[i] in opening_chars:
					expected_closing.append(closing_chars[line[i]])
				elif line[i] == expected_closing[-1]:
					expected_closing.pop(-1)
				else:
					status = "corrupted"
					illegal_chars[line[i]] += 1 
					break
			if len(expected_closing) > 0 and status != "corrupted":
				status="incomplete"
			line_status.append(status)
		else:
			line_status.append("corrupted")

	score = 0
	for c in closing_chars.values():
		score += illegal_chars[c] * points[c]
	print(score)

if __name__ == "__main__":
	main()